package dev.rehm.controller;

import dev.rehm.domain.Student;
import dev.rehm.service.StudentService;
import dev.rehm.service.StudentServiceImpl;
import org.junit.Test;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StudentControllerTest {
    private StudentService service = new StudentServiceImpl();
@Test
    public void findAll(){
        ArrayList<Student> list = service.findAll();
        for(Student stu : list){
            System.out.println(stu);
        }
    }

@Test
    public void findById(){
        Student stu = service.findById(1);
        System.out.println(stu);
    }

@Test
    public void insert() {

    Student stu = new Student(7,"seven", 20);
    int result = service.insertStudent(stu);

    if(result != 0){
        System.out.println("successfully inserted");
    }else{
        System.out.println("inserted failed");
    }
}

@Test
    public void update(){
    Student stu = service.findById(7);
    stu.setName("twenty");


    int result = service.updateStudent(stu);

    if(result != 0){
        System.out.println("successfully updated");
    }else {
        System.out.println("update failed");
    }
}

@Test
    public void delete(){
    int result = service.deleteById(7);

    if(result != 0){
        System.out.println("successfully deleted ");
    }else {
        System.out.println("deleted failed");
    }
}
}
