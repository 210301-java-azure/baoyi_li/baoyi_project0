package dev.rehm.service;

import dev.rehm.dao.AccountDao;
import dev.rehm.domain.Account;

public class AccountService {
    AccountDao dao = new AccountDao();
    public Account verify(String username){
        return dao.verifyAccount(username);
    }
}
