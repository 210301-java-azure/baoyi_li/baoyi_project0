package dev.rehm.controller;

import dev.rehm.domain.Account;
import dev.rehm.service.AccountService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthController {
    private Logger logger = LoggerFactory.getLogger(AuthController.class);

    public void authenticateLogin(Context ctx){
        //
        AccountService service= new AccountService();
        String user = ctx.formParam("username");
        String password = ctx.formParam("password");
        logger.info(user +" attempted login");

        Account account = service.verify(user);
        if(account!=null && user.equals(account.getUsername()) && password.equals(account.getPassword())){
            logger.info("successful login");
            // send back token
            ctx.header("Authorization", "user-auth-token");
            ctx.status(200);
            return;
        }
//        if(user!=null && user.equals("Ken")){
//            if(password!=null && password.equals("teacherOnly")){
//                logger.info("successful login");
//                // send back token
//                ctx.header("Authorization", "user-auth-token");
//                ctx.status(200);
//                return;
//            }
//        }
        throw new UnauthorizedResponse("Authentication Failed");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        // getting the "Authorization" header from the incoming request
        String authHeader = ctx.header("Authorization");

        if(authHeader!=null && authHeader.equals("user-auth-token")){
            logger.info("request is authorized, proceeding to handler method");
        } else {
            logger.warn("improper authorization");
            throw new UnauthorizedResponse();
        }
    }

}
