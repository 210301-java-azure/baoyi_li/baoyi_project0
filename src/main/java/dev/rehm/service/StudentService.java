package dev.rehm.service;

import dev.rehm.domain.Student;

import java.util.ArrayList;

public interface StudentService {
    public abstract ArrayList<Student> findAll();

    public abstract Student findById (Integer id);

    public abstract int insertStudent(Student stu);

    public abstract int updateStudent(Student Stu);

    public abstract int deleteById(Integer id);
}
