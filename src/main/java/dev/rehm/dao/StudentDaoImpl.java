package dev.rehm.dao;

import dev.rehm.domain.Student;
import dev.rehm.utils.JDBCUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StudentDaoImpl implements StudentDao{

    @Override
    public ArrayList<Student> findAll() {
        ArrayList<Student> list = new ArrayList<>();
        Connection con = null;
        Statement stat = null;
        ResultSet result = null;
        try{
            con = JDBCUtils.getConnection();
            stat = con.createStatement();

            String sql = "SELECT * FROM student";
            result = stat.executeQuery(sql);

            while(result.next()){
                Integer sid = result.getInt("sid");
                String name = result.getString("name");
                Integer age = result.getInt("age");

                Student stu = new Student(sid,name,age);
                list.add(stu);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(con, stat, result);
        }
        return list;
    }

    @Override
    public Student findById(Integer id) {
        Connection con = null;
        PreparedStatement preStat = null;
        ResultSet result = null;
        Student stu = new Student();
        try{

            con = JDBCUtils.getConnection();
            String sql = "SELECT * FROM student WHERE sid = ?";
            preStat = con.prepareStatement(sql);
            preStat.setInt(1,id);
            result = preStat.executeQuery();

            if (result.next()){
                Integer sid = result.getInt("sid");
                String name = result.getString("name");
                Integer age = result.getInt("age");


                stu.setSid(sid);
                stu.setName(name);
                stu.setAge(age);

                return stu;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(con, preStat,result);
        }
        return null;
    }

    @Override
    public int insertStudent(Student stu) {
        Connection con = null;
        PreparedStatement preStat = null;
        int result = 0;
        try{
            con = JDBCUtils.getConnection();
            //stat = con.createStatement();
            String sql = "INSERT INTO student (sid, name, age) VALUES (?,?,?)";
            preStat = con.prepareStatement(sql);

            preStat.setInt(1,stu.getSid());
            preStat.setString(2, stu.getName());
            preStat.setInt(3, stu.getAge());

            result = preStat.executeUpdate();



        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(con, preStat);
        }
        return result;
    }

    @Override
    public int updateStudent(Student stu) {
        Connection con = null;
        PreparedStatement preStat = null;
        int result = 0;
        try{
            con = JDBCUtils.getConnection();
            //stat = con.createStatement();
            String sql = "UPDATE student SET name=?, age=? WHERE sid=?";

            preStat = con.prepareStatement(sql);

            preStat.setString(1, stu.getName());
            preStat.setInt(2, stu.getAge());
            /*
            parse sql date
             */

            preStat.setInt(3,stu.getSid());
            result = preStat.executeUpdate();



        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(con,preStat);
        }
        return result;
    }

    @Override
    public int deleteById(Integer sid) {
        Connection con = null;
        Statement stat = null;
        int result = 0;
        try{
            con = JDBCUtils.getConnection();
            stat = con.createStatement();

            String sql = "DELETE FROM student WHERE sid ='"+sid+"'";
            result = stat.executeUpdate(sql);


        }catch (Exception e){
            e.printStackTrace();
        }finally {
           JDBCUtils.close(con, stat);
        }
        return result;
    }

}
