package dev.rehm.utils;

import java.io.InputStream;
import java.sql.*;
import java.util.Properties;


/*
JDBC connection util
 */
public class JDBCUtils {
    /*
    1
     */
    private JDBCUtils(){ }
    /*

     */
    private static String driverClass = "org.postgresql.Driver";
    private static String url = "jdbc:postgresql://baoyi-java-azure-training.postgres.database.azure.com:5432/postgres";
    private static String username ="baoyi@baoyi-java-azure-training";
    private static String password = "azure123@";
    private static Connection con;

    /*

     */
    static{
        try {

            Class.forName(driverClass);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*

     */
    public static Connection getConnection() {
        try {
            con = DriverManager.getConnection(url,username,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return con;
    }

    /*

     */
    public static void close(Connection con, Statement stat, ResultSet rs) {
        if(con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(stat != null) {
            try {
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection con, PreparedStatement psta, ResultSet rs) {
        if(con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(psta != null) {
            try {
                psta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static void close(Connection con, Statement stat) {
        if(con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(stat != null) {
            try {
                stat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection con, PreparedStatement psta) {
        if(con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(psta != null) {
            try {
                psta.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
