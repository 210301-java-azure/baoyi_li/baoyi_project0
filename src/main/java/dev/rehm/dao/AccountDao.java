package dev.rehm.dao;

import dev.rehm.domain.Account;
import dev.rehm.domain.Student;
import dev.rehm.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class AccountDao {
    public Account verifyAccount(String username){
        Account account=new Account();
        Connection con = null;
        PreparedStatement stat = null;
        ResultSet result = null;
        try{
            con = JDBCUtils.getConnection();

            String sql = "select * from account where username = ?";
            stat = con.prepareStatement(sql);
            stat.setString(1,username);
            result = stat.executeQuery();
            if (result.next()){
                String user = result.getString("username");
                String pass = result.getString("password");
                account.setUsername(user);
                account.setPassword(pass);
                return account;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            JDBCUtils.close(con, stat, result);
        }
        return null;
    }
}
