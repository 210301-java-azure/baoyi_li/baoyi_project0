package dev.rehm;

import dev.rehm.controller.AuthController;
import dev.rehm.controller.StudentController;
import dev.rehm.domain.Student;
import dev.rehm.service.StudentService;
import dev.rehm.service.StudentServiceImpl;
import io.javalin.Javalin;
import io.javalin.http.Context;

import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000);
        app.get("/", (Context ctx)-> {ctx.result("Student Manage System");});
//        app.get("/students", ctx->{
//            StudentService service = new StudentServiceImpl();
//            ArrayList<Student> list = service.findAll();
//            //ctx.result(data.getAllItems().toString());}
//            ctx.json(list);});

        StudentController controller = new StudentController();
        AuthController autoController = new AuthController();

        app.get("/login/students",controller::handleGetStudentsRequest);
        app.get("/login/students/:id",controller::handleGetStudentBySidRequest);

        app.post("/login/students", controller::handlePostNewStudent);

        app.delete("/login/students/:id", controller::handleDeleteById);

        app.put("/login/students/:id", controller::handleUpdateByID);

        app.post("/login", autoController::authenticateLogin );

        app.before("/login/*",autoController::authorizeToken);
    }
}
