drop table if exists student;
create table student(
	sid serial primary key,
	name varchar(20) not null,
	age int
	
);
insert into public.student values(default, 'Vance', 20);
insert into public.student values(default, 'Briana', 21);
insert into public.student values(default, 'Lorena', 19);
insert into public.student values(default, 'Nam', 20);
insert into public.student values(default, 'Glen', 19);

select * from student;
select * from student where sid = 100;

drop table if exists account;
create table account(
	id serial primary key,
	username varchar(20) not null,
	password varchar(20) not null
);
insert into account values(default, 'Ken', 'KenPassword');
insert into account values(default, 'John', 'JohnPassword');
insert into account values(default, 'Anna', 'AnnaPassword');
insert into account values(default, 'Lynn', 'LynnPassword');
select * from account where username = 'Ken';
select * from account;