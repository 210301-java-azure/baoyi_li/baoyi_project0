package dev.rehm.dao;

import dev.rehm.domain.Student;

import java.util.ArrayList;

public interface StudentDao {
    public abstract ArrayList<Student> findAll();

    public abstract Student findById(Integer id);

    public abstract int insertStudent(Student stu);

    public abstract int updateStudent(Student stu);

    public abstract int deleteById(Integer id);
}
