package dev.rehm.service;

import dev.rehm.dao.StudentDao;
import dev.rehm.dao.StudentDaoImpl;
import dev.rehm.domain.Student;

import java.util.ArrayList;

public class StudentServiceImpl implements StudentService{
    private StudentDao dao = new StudentDaoImpl();
    @Override
    public ArrayList<Student> findAll() {
        return dao.findAll();
    }

    @Override
    public Student findById(Integer id) {
        return dao.findById(id);
    }

    @Override
    public int insertStudent(Student stu) {
        return dao.insertStudent(stu);
    }

    @Override
    public int updateStudent(Student stu) {
        return dao.updateStudent(stu);
    }

    @Override
    public int deleteById(Integer id) {
        return dao.deleteById(id);
    }
}
